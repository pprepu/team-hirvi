FROM node:16-alpine

# ARG PG_HOST
# ARG PG_PORT
# ARG PG_USERNAME
# ARG PG_PASSWORD
# ARG PG_DATABASE
# ARG PORT

# ENV PG_HOST=${PG_HOST}
# ENV PG_PORT=${PG_PORT}
# ENV PG_USERNAME=${PG_USERNAME}
# ENV PG_PASSWORD=${PG_PASSWORD}
# ENV PG_DATABASE=${PG_DATABASE}
# ENV PORT=${PORT}

WORKDIR /app
COPY ./backend/package*.json ./
RUN npm install

COPY ./backend/src/*.js ./src/
COPY ./frontend/dist ./dist

EXPOSE 3000
CMD ["npm", "start"]
