import { Router } from "express";

const router = Router();
// code here:

let todos = [];
let todoID = 0;

const generateId = () => {
  todoID = todoID + 1;
  return todoID;
};

router.get("/", (req, res) => {
  res.status(200).send(todos);
});

router.post("/", (req, res) => {
  const { todo } = req.body;

  if (!todo) {
    return res.status(404).send("Invalid todo");
  }

  const id = generateId(todoID);
  const newTodo = { id, todo };
  todos = todos.concat(newTodo);

  res.status(201).send(newTodo);
});

router.delete("/:id", (req, res) => {
  const id = Number(req.params.id);

  const updatedTodos = todos.filter((todo) => todo.id !== id);
  todos = updatedTodos;

  res.status(201).send(updatedTodos);
});

export default router;
