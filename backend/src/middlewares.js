export const validateFields = (req, res, next) => {
  const { value1, value2 } = req.body

  if (!value1 || value2) {
    return res.status(400).send('fields missing')
  }

  next()
}

export const logger = (req, res, next) => {
  console.log(`${req.method} -> ${req.path}`)
  next()
}

export const authenticate = (req, res, next) => {

  next()
}