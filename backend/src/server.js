import express from 'express'
import cors from 'cors'
import todoRouter from './todoRouter.js'

import { logger } from './middlewares.js'
const server = express()

const versionNumber = 0.002

server.use(cors())
server.use(express.json())

// server.use(logger)
server.use(express.static('dist'))

server.use('/todos', todoRouter)
 
server.get('/version', (req, res) => {
  res.send(`${versionNumber}`)
})

export default server