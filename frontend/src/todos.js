const urlForTodos = "/todos";

const getTodos = async () => {
  const response = await fetch(urlForTodos);
  const todos = await response.json();
  return todos;
};

const createTodos = async (todo) => {
  const config = {
    method: "post",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({ todo }),
  };

  const response = await fetch(urlForTodos, config);
  const newTodo = await response.json();
  return newTodo;
};

const deleteTodos = async (id) => {
  const config = {
    method: "delete",
  };

  const response = await fetch(`${urlForTodos}/${id}`, config);
  const updatedTodos = await response.json();
  return updatedTodos;
};

export default { getTodos, createTodos, deleteTodos };
