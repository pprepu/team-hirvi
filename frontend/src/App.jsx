import { useState, useEffect } from "react";
import "./App.css";
import todoService from "./todos";

const versionUrl = "/version";

let uniqueId = 100;

function App() {
  const [version, setVersion] = useState(0);
  const [list, setList] = useState([]);
  const [input, setInput] = useState("");

  const fetchNewVersion = async () => {
    const response = await fetch(versionUrl);
    const newVersion = await response.text();
    console.log(newVersion)
    setVersion(newVersion);

  };

  const fetchTodos = async () => {
    const todosFetched = await todoService.getTodos();
    console.log(todosFetched)
    setList(todosFetched);
  };

  useEffect(() => {
    fetchNewVersion();
    fetchTodos();
  }, []);

  const addItem = async () => {
    if (!input) return null;

    const newTodo = await todoService.createTodos(input);
    console.log(newTodo)
    setList([...list, newTodo]);

    setInput("");
  };

  const toggle = (id) => () => {
    setList(
      list.map((item) =>
        item.id === id ? { ...item, isDone: !item.isDone } : item
      )
    );
  };

  const remove = async (id) => {
    const updatedTodos = await todoService.deleteTodos(id);
    setList(updatedTodos);
  };

  return (
    <div className="App">
      <h1>Todos</h1>
      <input
        className="todo-input"
        value={input}
        onChange={(event) => setInput(event.target.value)}
      />
      <button onClick={addItem}>Add</button>
      <ul>
        {list.map(({ id, todo }) => {
          return (
            <li key={id} className="item">
              <span>{todo}</span>
              <button className="icon" onClick={() => remove(id)}>
                remove
              </button>
            </li>
          );
        })}
      </ul>
      <div>version {version}</div>
    </div>
  );
}

export default App;
