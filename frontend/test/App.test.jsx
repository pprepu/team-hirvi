import { describe, it, expect, beforeEach } from "vitest";
import { render, screen, act, waitFor } from "@testing-library/react";

import App from "../src/App";
import userEvent from "@testing-library/user-event";
const textForTodoItem = "myTodo";
describe("App component", () => {

  it('renders a h1 element with the text "Todos"', async () => {
    fetch
      .once('0.2')
      .once(JSON.stringify([]))


    await act(() => render(<App />));
    const h1Element = screen.getByRole("heading", { level: 1 });
    expect(h1Element).toHaveTextContent("Todos");
  });

  it("renders a new todo after it is created by the user", async () => {
    fetch
      .once('0.2')
      .once(JSON.stringify([]))
      .once(JSON.stringify({ id: 1, todo: textForTodoItem }))

    await act(() => render(<App />));
    const user = userEvent.setup();
    const inputElement = screen.getByRole("textbox");
    const buttonElement = screen.getByRole("button");

    await user.type(inputElement, textForTodoItem);
    await user.click(buttonElement);
    await waitFor(() => {
      const todoItems = screen.getAllByRole("listitem");

      expect(todoItems).toHaveLength(1);

      expect(todoItems[0]).toHaveClass("item");
      expect(todoItems[0]).toHaveTextContent(textForTodoItem);
      // screen.debug()
    })

  });

  // it('removing a todo works', async () => {
  //   const textForTodoItem = 'myTodo'

  //   const user = userEvent.setup()
  //   const inputElement = screen.getByRole('textbox')
  //   const buttonElement = screen.getByRole('button')

  //   await user.type(inputElement, textForTodoItem)
  //   await user.click(buttonElement)

  //   const todoItems = screen.getAllByRole('listitem')
  //   // expect(todoItems.length).not.toBe(0)

  //   expect(todoItems).toHaveLength(1)

  //   expect(todoItems[0]).not.toHaveClass('checked')
  //   expect(todoItems[0]).toHaveClass('item')
  //   expect(todoItems[0]).toHaveTextContent(textForTodoItem)

  //   const removeButtonElement = screen.getByText('remove')

  //   expect(removeButtonElement).toHaveTextContent('remove')

  //   await user.click(removeButtonElement)

  //   const todoItemsAfterRemove = screen.queryAllByRole('listitem')
  //   expect(todoItemsAfterRemove).toHaveLength(0)

  // })

  // it('it is possible to toggle between "done" and "undone" todos', async () => {
  //   const textForTodoItem = 'myTodo'

  //   const user = userEvent.setup()
  //   const inputElement = screen.getByRole('textbox')
  //   const buttonElement = screen.getByRole('button')

  //   await user.type(inputElement, textForTodoItem)
  //   await user.click(buttonElement)

  //   const todoItems = screen.getAllByRole('listitem')
  //   // expect(todoItems.length).not.toBe(0)

  //   expect(todoItems).toHaveLength(1)

  //   const todoItem = todoItems[0]

  //   expect(todoItem).toHaveClass('item')
  //   expect(todoItem).toHaveTextContent(textForTodoItem)

  //   const todoSpan = screen.getByText(textForTodoItem)

  //   await user.click(todoSpan)

  //   const todoSpanAfterClick = screen.getByText(textForTodoItem)
  //   expect(todoSpanAfterClick).toHaveClass('done')

  // })
});
